using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.zadatak
            string filePath = @"C:\Users\hrco\Desktop\2. zadatak.txt";
            Dataset dataset = new Dataset(filePath);
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapt = new Adapter(analyzer);
            foreach (var rezultat in adapt.CalculateAveragePerColumn(dataset))
            {
                Console.Write(rezultat + "\t");
            }
            Console.WriteLine();
            foreach (var rezultat in adapt.CalculateAveragePerRow(dataset))
            {
                Console.Write(rezultat + "\t");
            }
            //3.zadatak
            RentingConsolePrinter Print = new RentingConsolePrinter();
            knjiga book = new knjiga("Game of Thrones");
            Video video = new Video("Inception");
            //4.zadatak
            HotItemcs hotBook = new HotItemcs(new knjiga("Harry Potter and the Goblet of Fire"));
            HotItemcs hotVideo = new HotItemcs(new Video("The Shawshank Redemption"));
            List<IRentable> rentable = new List<IRentable>();
            rentable.Add(book);
            rentable.Add(video);
            rentable.Add(hotBook);
            rentable.Add(hotVideo);
            Print.PrintTotalPrice(rentable);
            Print.DisplayItems(rentable);

        }
    }
}